package configuration

import (
	"fmt"

	"github.com/jinzhu/gorm"
	"gitlab.com/grab-whatever/promo-manager/env"
	"gitlab.com/grab-whatever/promo-manager/model"

	_ "github.com/jinzhu/gorm/dialects/postgres"
)

var (
	host     = env.GetEnv("DB_HOST", "localhost")
	user     = env.GetEnv("DB_USER", "promo_manager")
	name     = env.GetEnv("DB_NAME", "promo_manager")
	port     = env.GetEnv("DB_PORT", "5432")
	password = env.GetEnv("DB_PASSWORD", "password")
)

// NewPostgresConnection return new connection to DB and execute migration.
func NewPostgresConnection() (*gorm.DB, error) {
	connectStr := fmt.Sprintf(
		"host=%s port=%s user=%s dbname=%s password=%s sslmode=disable",
		host,
		port,
		user,
		name,
		password,
	)

	db, err := gorm.Open("postgres", connectStr)
	if err != nil {
		return nil, err
	}

	db.DB().SetConnMaxLifetime(0)
	db.Debug()

	db.AutoMigrate(model.Promotion{})

	return db, nil
}
