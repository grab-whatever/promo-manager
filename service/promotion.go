package service

import (
	"gitlab.com/grab-whatever/promo-manager/model"
	"gitlab.com/grab-whatever/promo-manager/pkg"
	"gitlab.com/grab-whatever/promo-manager/repository"
)

type PromotionRepository interface {
	Create(promotion *model.Promotion) error
	Update(id uint, promoData map[string]interface{}) error
	Get(id uint) (*model.Promotion, error)
	GetAll() ([]*model.Promotion, error)
	Delete(id uint) error
}

type service struct {
	promotionRepository PromotionRepository
}

func NewPromotionService(promoRepository PromotionRepository) *service {
	return &service{promotionRepository: promoRepository}
}

func (ps service) Create(promoData map[string]interface{}) (uint, error) {
	promotion := model.NewPromotion()
	if err := promotion.SetModelFrom(promoData); err != nil {
		return 0, err
	}

	err := ps.promotionRepository.Create(promotion)
	if err != nil {
		return 0, processError(err)
	}
	return promotion.ID, nil
}

func (ps service) Update(id uint, promoData map[string]interface{}) error {
	err := ps.promotionRepository.Update(id, promoData)
	if err != nil {
		return processError(err)
	}
	return nil
}

func (ps service) Get(id uint) (*model.Promotion, error) {
	promotion, err := ps.promotionRepository.Get(id)
	if err != nil {
		return nil, processError(err)
	}
	return promotion, nil
}

func (ps service) GetAll() ([]*model.Promotion, error) {
	promotions, err := ps.promotionRepository.GetAll()
	if err != nil {
		return nil, processError(err)
	}
	return promotions, nil
}

func (ps service) Delete(id uint) error {
	if err := ps.promotionRepository.Delete(id); err != nil {
		return processError(err)
	}
	return nil
}

func processError(err error) error {
	if repository.IsDBRecordNotFound(err) {
		return pkg.NewNotFoundError()
	}
	return err
}
