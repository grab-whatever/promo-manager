package controller

import (
	"encoding/json"
	"io"
	"log"
	"net/http"
)

const (
	BasePath = "/api/promo-manager"
)

type Response struct {
	Success bool        `json:"success"`
	Message string      `json:"message,omitempty"`
	Data    interface{} `json:"data,omitempty"`
}

func responseWrapperFilter(h func(io.Writer, *http.Request) (interface{}, int, error)) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		data, status, err := h(w, r)
		if err != nil {
			data = err.Error()
		}
		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(status)
		if status != http.StatusNoContent {
			res := Response{
				Data:    data,
				Success: err == nil,
			}
			if err != nil {
				res.Message = err.Error()
			}
			if err := json.NewEncoder(w).Encode(res); err != nil {
				log.Printf("could not encode Response to output: %v", err)
			}
		}
	}
}
