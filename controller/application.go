package controller

import (
	"fmt"
	"io"
	"net/http"

	muxRouter "github.com/gorilla/mux"
	"github.com/jinzhu/gorm"
	"gitlab.com/grab-whatever/promo-manager/model"
	"gitlab.com/grab-whatever/promo-manager/pkg"
	"gitlab.com/grab-whatever/promo-manager/repository"
	"gitlab.com/grab-whatever/promo-manager/service"
)

type Application struct {
	DB          *gorm.DB
	HTTPHandler http.Handler
}

func (a *Application) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	a.HTTPHandler.ServeHTTP(w, r)
}

type promotionHandler struct {
	promotionService promotionService
}

type promotionService interface {
	Create(promoData map[string]interface{}) (uint, error)
	Update(id uint, promoData map[string]interface{}) error
	Get(id uint) (*model.Promotion, error)
	GetAll() ([]*model.Promotion, error)
	Delete(id uint) error
}

func NewApplication(db *gorm.DB) *Application {
	promotionRepository := repository.NewPromotionRepository(db)

	promotionService := service.NewPromotionService(promotionRepository)

	promotionHTTPHandler := newPromotionHandler(promotionService)

	return &Application{
		DB:          db,
		HTTPHandler: promotionHTTPHandler,
	}
}

func newPromotionHandler(promotionService promotionService) http.Handler {
	handler := &promotionHandler{
		promotionService: promotionService,
	}

	mux := muxRouter.NewRouter()

	// Promotion endpoints
	mux.HandleFunc(fmt.Sprintf("%v/promotions", BasePath), responseWrapperFilter(handler.CreatePromotion)).Methods("POST")
	mux.HandleFunc(fmt.Sprintf("%v/promotions", BasePath), responseWrapperFilter(handler.GetPromotions)).Methods("GET")
	mux.HandleFunc(fmt.Sprintf("%v/promotions/{id:[0-9]+}", BasePath), responseWrapperFilter(handler.UpdatePromotion)).Methods("PATCH")
	mux.HandleFunc(fmt.Sprintf("%v/promotions/{id:[0-9]+}", BasePath), responseWrapperFilter(handler.GetPromotion)).Methods("GET")
	mux.HandleFunc(fmt.Sprintf("%v/promotions/{id:[0-9]+}", BasePath), responseWrapperFilter(handler.DeletePromotion)).Methods("DELETE")

	return mux
}

func (ph *promotionHandler) CreatePromotion(w io.Writer, r *http.Request) (interface{}, int, error) {
	bodyMap, err := pkg.GetBodyFrom(r)
	if err != nil {
		return nil, http.StatusBadRequest, err
	}

	promoID, err := ph.promotionService.Create(bodyMap)
	if err != nil {
		return nil, pkg.TranslateErrorToHTTPStatus(err), err
	}

	return promoID, http.StatusCreated, nil
}

func (ph *promotionHandler) UpdatePromotion(w io.Writer, r *http.Request) (interface{}, int, error) {
	id, err := pkg.GetIDFrom(r)
	if err != nil {
		return nil, http.StatusBadRequest, err
	}

	bodyMap, err := pkg.GetBodyFrom(r)
	if err != nil {
		return nil, http.StatusBadRequest, err
	}

	if err := ph.promotionService.Update(id, bodyMap); err != nil {
		return nil, pkg.TranslateErrorToHTTPStatus(err), err
	}

	return nil, http.StatusNoContent, nil
}

func (ph *promotionHandler) GetPromotion(w io.Writer, r *http.Request) (interface{}, int, error) {
	id, err := pkg.GetIDFrom(r)
	if err != nil {
		return nil, http.StatusBadRequest, err
	}
	promotion, err := ph.promotionService.Get(id)
	if err != nil {
		return nil, pkg.TranslateErrorToHTTPStatus(err), err
	}

	return promotion, http.StatusOK, nil
}

func (ph *promotionHandler) DeletePromotion(w io.Writer, r *http.Request) (interface{}, int, error) {
	id, err := pkg.GetIDFrom(r)
	if err != nil {
		return nil, http.StatusBadRequest, err
	}
	if err := ph.promotionService.Delete(id); err != nil {
		return nil, pkg.TranslateErrorToHTTPStatus(err), err
	}

	return nil, http.StatusNoContent, nil
}

func (ph *promotionHandler) GetPromotions(w io.Writer, r *http.Request) (interface{}, int, error) {
	promotions, err := ph.promotionService.GetAll()
	if err != nil {
		return nil, pkg.TranslateErrorToHTTPStatus(err), err
	}

	return promotions, http.StatusOK, nil
}
