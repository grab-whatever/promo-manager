package repository

import "github.com/jinzhu/gorm"

func IsDBRecordNotFound(err error) bool {
	return err != nil && gorm.IsRecordNotFoundError(err)
}
