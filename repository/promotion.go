package repository

import (
	"github.com/jinzhu/gorm"
	"gitlab.com/grab-whatever/promo-manager/model"
)

type repository struct {
	db *gorm.DB
}

func NewPromotionRepository(db *gorm.DB) *repository {
	return &repository{db: db}
}

func (pr *repository) Create(promotion *model.Promotion) error {
	return pr.db.Create(promotion).Error
}

func (pr *repository) Update(id uint, promoData map[string]interface{}) error {
	return pr.db.Transaction(func(tx *gorm.DB) error {
		existing := &model.Promotion{
			Model: gorm.Model{ID: id},
		}
		if err := tx.First(existing).Error; err != nil {
			return err
		}
		if err := existing.SetModelFrom(promoData); err != nil {
			return err
		}

		if err := tx.Save(existing).Error; err != nil {
			return err
		}

		return nil
	})
}

func (pr *repository) Get(id uint) (*model.Promotion, error) {
	promo := &model.Promotion{
		Model: gorm.Model{ID: id},
	}
	if err := pr.db.First(promo).Error; err != nil {
		return nil, err
	}
	return promo, nil
}

func (pr *repository) GetAll() ([]*model.Promotion, error) {
	var promotions []*model.Promotion
	if err := pr.db.Find(&promotions).Error; err != nil {
		return nil, err
	}
	return promotions, nil
}

func (pr *repository) Delete(id uint) error {
	promo := &model.Promotion{
		Model: gorm.Model{ID: id},
	}
	if err := pr.db.Delete(promo).Error; err != nil {
		return err
	}
	return nil
}
