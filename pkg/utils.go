package pkg

import (
	"encoding/json"
	"io/ioutil"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
	"github.com/pkg/errors"
)

func GetBodyFrom(r *http.Request) (map[string]interface{}, error) {
	b, err := ioutil.ReadAll(r.Body)
	if err != nil {
		return nil, errors.New("json request body is malformed")
	}

	if len(b) == 0 {
		return nil, errors.New("json request body is empty")
	}

	var body map[string]interface{}
	if err := json.Unmarshal(b, &body); err != nil {
		return nil, errors.New("error when performing unmarshaling, the json request body is malformed")
	}
	return body, nil
}

func GetIDFrom(r *http.Request) (uint, error) {
	vars := mux.Vars(r)
	id, ok := vars["id"]
	if !ok {
		return 0, errors.Errorf("there is no ID specified for this request")
	}

	id64, err := strconv.ParseUint(id, 10, 64)
	if err != nil {
		return 0, errors.Wrap(err, "provided promo id is not a valid id")
	}
	return uint(id64), nil
}
