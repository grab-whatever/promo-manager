package pkg

import (
	"testing"
	"time"
)

func Test_ConvertStringDatesToTimeWithin(t *testing.T) {
	now := time.Now()
	nowString := now.Format(time.RFC3339)

	type args struct {
		data map[string]interface{}
		keys []string
	}
	tests := []struct {
		name    string
		args    args
		wantErr bool
	}{
		{
			name: "success path",
			args: args{
				data: map[string]interface{}{
					"someString":    "what-ever",
					"aNumber":       4,
					"aDateAsString": nowString,
				},
				keys: []string{"aDateAsString"},
			},
			wantErr: false,
		},
		{
			name: "malformed date missing seconds component",
			args: args{
				data: map[string]interface{}{
					"someString":    "what-ever",
					"aNumber":       4,
					"aDateAsString": "2006-01-02T15:04:Z07:00",
				},
				keys: []string{"aDateAsString"},
			},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := ConvertStringDatesToTimeWithin(tt.args.data, tt.args.keys...); (err != nil) != tt.wantErr {
				t.Errorf("parseStringDatesToTimeFrom() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}
