package pkg

import (
	"fmt"
	"time"

	"github.com/pkg/errors"
)

func ConvertStringDatesToTimeWithin(data map[string]interface{}, keys ...string) error {
	if len(keys) == 0 {
		fmt.Print("there were no keys to parse data from\n")
		return nil
	}

	for _, key := range keys {
		aux, ok := data[key]
		if !ok {
			fmt.Printf("there is no %v key to convert to date time\n", key)
			continue
		}

		stringTime, ok := aux.(string)
		if !ok {
			return fmt.Errorf("there was an error when trying to get the string value from the key %v", key)
		}

		actualTime, err := time.Parse(time.RFC3339, stringTime)
		if err != nil {
			errMsg := fmt.Sprintf("there was an error when trying to format date from key %v \n", key)
			return errors.Wrap(err, errMsg)
		}

		data[key] = actualTime
	}

	return nil
}
