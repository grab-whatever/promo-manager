package pkg

import (
	"fmt"
	"net/http"
	"strings"
)

type NotFoundError struct{}

func NewNotFoundError() *NotFoundError {
	return &NotFoundError{}
}

func (n NotFoundError) Error() string {
	return "the required data has not been found"
}

func TranslateErrorToHTTPStatus(err error) int {
	switch t := err.(type) {
	case NotFoundError:
		return http.StatusNotFound
	case DataValidationError:
		return http.StatusBadRequest
	default:
		fmt.Printf("defaulting to internal server error with type %v", t)
		return http.StatusInternalServerError
	}
}

type DataValidationError struct {
	errors []error
}

func NewValidationError() *DataValidationError {
	return &DataValidationError{
		errors: []error{},
	}
}

func NewValidationErrorWith(err error) *DataValidationError {
	return &DataValidationError{
		errors: []error{err},
	}
}

func (v *DataValidationError) Add(err ...error) {
	v.errors = append(v.errors, err...)
}

func (v DataValidationError) HasAny() bool {
	return len(v.errors) > 0
}

func (v DataValidationError) Error() string {
	msgs := make([]string, len(v.errors))
	for i, e := range v.errors {
		msgs[i] = e.Error()
	}
	return strings.Join(msgs, "\n")
}
