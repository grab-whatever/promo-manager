FROM golang:1.13.4  as builder
WORKDIR /go/src/gitlab.com/grab-whatever/promo-manager
COPY go.mod .
COPY go.sum .
RUN go mod download
COPY . .
RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o app .

FROM scratch
#RUN apk --no-cache add ca-certificates
COPY --from=builder /go/src/gitlab.com/grab-whatever/promo-manager .
CMD ["./app"]