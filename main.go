package main

import (
	"context"
	"fmt"
	"log"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"

	"gitlab.com/grab-whatever/promo-manager/configuration"
	"gitlab.com/grab-whatever/promo-manager/controller"
	"gitlab.com/grab-whatever/promo-manager/env"
)

func main() {
	db, err := configuration.NewPostgresConnection()
	if err != nil {
		log.Fatalf("postgers: %v", err)
	}
	defer db.Close()

	application := controller.NewApplication(db)

	server := &http.Server{
		Addr:         fmt.Sprintf(":%s", env.GetEnv("PORT", "3000")),
		Handler:      application.HTTPHandler,
		ReadTimeout:  5 * time.Second,
		WriteTimeout: 10 * time.Second,
		IdleTimeout:  120 * time.Second,
	}

	go func() {
		log.Printf("Starting HTTP Server. Listening at %q", server.Addr)
		if err := server.ListenAndServe(); err != http.ErrServerClosed {
			log.Fatalf("%v", err)
		} else {
			log.Println("Server closed!")
		}
	}()

	// Check for a closing signal
	// Graceful shutdown
	sigquit := make(chan os.Signal, 1)
	signal.Notify(sigquit, os.Interrupt, syscall.SIGTERM)
	sig := <-sigquit
	log.Printf("caught sig: %+v", sig)
	log.Printf("Gracefully shutting down server...")

	if err := server.Shutdown(context.Background()); err != nil {
		log.Printf("Unable to shut down server: %v", err)
	} else {
		log.Println("Server stopped")
	}
}
