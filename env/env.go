package env

import (
	"log"
	"os"
)

// GetEnv return the value of the env or the fallback defined if the env does not exist
func GetEnv(name, fallback string) string {
	env, ok := os.LookupEnv(name)
	if !ok {
		env = fallback
	}

	return env
}

// MustEnv return an env and the process will exit if does not exist
func MustEnv(name string) string {
	env, ok := os.LookupEnv(name)
	if !ok {
		log.Fatalf("env %v is required", name)
	}

	return env
}
