module gitlab.com/grab-whatever/promo-manager

go 1.14

require (
	github.com/asaskevich/govalidator v0.0.0-20200108200545-475eaeb16496
	github.com/gorilla/mux v1.7.4
	github.com/jinzhu/gorm v1.9.12
	github.com/mitchellh/mapstructure v1.2.2
	github.com/pkg/errors v0.9.1
	github.com/stretchr/objx v0.2.0 // indirect
	github.com/stretchr/testify v1.5.1
	google.golang.org/appengine v1.4.0
	gopkg.in/yaml.v2 v2.2.8 // indirect
)
