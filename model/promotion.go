package model

import (
	"time"

	validator "github.com/asaskevich/govalidator"
	"github.com/jinzhu/gorm"
	"github.com/mitchellh/mapstructure"
	"github.com/pkg/errors"
	"gitlab.com/grab-whatever/promo-manager/pkg"
)

type Promotion struct {
	gorm.Model
	BusinessUUID string     `gorm:"index" valid:"notnull,required"`
	Description  string     `valid:"notnull,required"`
	StartDate    *time.Time `gorm:"index"`
	EndDate      *time.Time `gorm:"index"`
}

func NewPromotion() *Promotion {
	return &Promotion{}
}

func (p *Promotion) SetModelFrom(data map[string]interface{}) error {
	vError := pkg.NewValidationError()
	if err := convertDate("startDate", data); err != nil {
		vError.Add(err)
	}

	if err := convertDate("endDate", data); err != nil {
		vError.Add(err)
	}

	if err := mapstructure.Decode(data, p); err != nil {
		vError.Add(err)
	}

	if vError.HasAny() {
		return vError
	}

	return nil
}

func (p Promotion) Validate() error {
	es := pkg.NewValidationError()

	if dateErrors := p.ValidateDates(); len(dateErrors) > 0 {
		es.Add(dateErrors...)
	}

	ok, err := validator.ValidateStruct(p)
	if !ok {
		es.Add(err)
	}

	if !es.HasAny() {
		return nil
	}

	return es
}

func (p *Promotion) BeforeSave() (err error) {
	if err := p.Validate(); err != nil {
		return err
	}
	return nil
}

func (p Promotion) ValidateDates() []error {
	var es []error
	if p.StartDate == nil && p.EndDate != nil {
		es = append(es, errors.New("by providing a end date it must exist a valid start date"))
	}

	compareDates := p.StartDate != nil && p.EndDate != nil
	if compareDates && p.StartDate.After(*p.EndDate) {
		es = append(es, errors.New("end date must not be before start date"))
	}

	return es
}

func convertDate(field string, data map[string]interface{}) error {
	_, ok := data[field]
	if ok {
		if err := pkg.ConvertStringDatesToTimeWithin(data, field); err != nil {
			return err
		}
	}
	return nil
}
