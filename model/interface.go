package model

type Validatable interface {
	Validate() error
}

type MapToModel interface {
	SetModelFrom(data map[string]interface{}) error
}

