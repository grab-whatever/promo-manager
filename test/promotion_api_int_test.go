package test

import (
	"bytes"
	"encoding/gob"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/grab-whatever/promo-manager/controller"
)

func Test_GetOnePromotion_Success(t *testing.T) {
	defer func() {
		// cleans DB up after test finishes up
		if err := TruncateTables(a.DB); err != nil {
			t.Errorf("error truncating test database tables: %v", err)
		}
	}()

	dbpromos, err := SeedPromotions(a.DB)
	if err != nil {
		t.Fatalf("error when seeding promotions: %v", err)
	}

	firstDBPromo := dbpromos[0]
	path := fmt.Sprintf("%v/promotions/%v", controller.BasePath, firstDBPromo.ID)
	response := buildAndExecuteRequest(http.MethodGet, path, nil, t)
	body := getBodyFromResponse(response, t)

	assert.Equal(t, 200, response.StatusCode)
	assert.Equal(t, true, body.Success)
}

func Test_GetOnePromotion_NotValidID(t *testing.T) {
	defer func() {
		// cleans DB up after test finishes up
		if err := TruncateTables(a.DB); err != nil {
			t.Errorf("error truncating test database tables: %v", err)
		}
	}()

	_, err := SeedPromotions(a.DB)
	if err != nil {
		t.Fatalf("error when seeding promotions: %v", err)
	}

	path := fmt.Sprintf("%v/promotions/%v", controller.BasePath, 99)
	response := buildAndExecuteRequest(http.MethodGet, path, nil, t)
	body := getBodyFromResponse(response, t)

	assert.Equal(t, 404, response.StatusCode)
	assert.Equal(t, false, body.Success)
}

func Test_GetAllPromotion_Success(t *testing.T) {
	defer func() {
		// cleans DB up after test finishes up
		if err := TruncateTables(a.DB); err != nil {
			t.Errorf("error truncating test database tables: %v", err)
		}
	}()

	_, err := SeedPromotions(a.DB)
	if err != nil {
		t.Fatalf("error when seeding promotions: %v", err)
	}

	path := fmt.Sprintf("%v/promotions", controller.BasePath)
	response := buildAndExecuteRequest(http.MethodGet, path, nil, t)
	body := getBodyFromResponse(response, t)

	assert.Equal(t, 200, response.StatusCode)
	assert.Equal(t, true, body.Success)
}

func buildAndExecuteRequest(httpMethod, basePath string, body []byte, t *testing.T) *http.Response {
	request, err := http.NewRequest(httpMethod, basePath, bytes.NewReader(body))
	if err != nil {
		t.Fatalf("could not create test request: %v", err)
	}

	recorder := httptest.NewRecorder()
	a.ServeHTTP(recorder, request)
	res := recorder.Result()
	defer res.Body.Close()

	return res
}

func getBodyFromResponse(res *http.Response, t *testing.T) *controller.Response {
	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		t.Fatal("there was an error when reading response body")
	}

	var r controller.Response
	err = json.Unmarshal(body, &r)
	if err != nil {
		t.Fatal("there was an error when unmashaling response body")
	}

	return &r
}

func GetBytes(key interface{}) ([]byte, error) {
	var buf bytes.Buffer
	enc := gob.NewEncoder(&buf)
	err := enc.Encode(key)
	if err != nil {
		return nil, err
	}
	return buf.Bytes(), nil
}
