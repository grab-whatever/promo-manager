package test

import (
	"log"
	"os"
	"testing"

	"gitlab.com/grab-whatever/promo-manager/configuration"
	"gitlab.com/grab-whatever/promo-manager/controller"
)

var a *controller.Application

func TestMain(m *testing.M) {
	os.Exit(testMain(m))
}

func testMain(m *testing.M) int {
	db, err := configuration.NewPostgresConnection()
	if err != nil {
		log.Fatalf("postgres: %v", err)
	}
	defer db.Close()

	a = controller.NewApplication(db)

	return m.Run()
}
