package test

import (
	"time"

	"github.com/jinzhu/gorm"
	"gitlab.com/grab-whatever/promo-manager/model"
)

func SeedPromotions(db *gorm.DB, mockFiles ...string) ([]*model.Promotion, error) {
	// TODO this could be loaded from different json files (mockFiles) for different scenarios, for now just hardcode data structs
	now := time.Now()
	anHourLater := now.Add(time.Hour)
	promotions := []*model.Promotion{
		{
			BusinessUUID: "uuid_1",
			Description:  "promo_1",
			StartDate:    &now,
			EndDate:      &anHourLater,
		},
		{
			BusinessUUID: "uuid_2",
			Description:  "promo_2",
			StartDate:    &now,
			EndDate:      &anHourLater,
		},
	}

	for _, p := range promotions {
		if err := db.Create(p).Error; err != nil {
			return nil, err
		}
	}

	var dbPromos []*model.Promotion
	if err := db.Find(&dbPromos).Error; err != nil {
		return nil, err
	}

	return dbPromos, nil
}

func TruncateTables(db *gorm.DB) error {
	stmt := "TRUNCATE TABLE promotions;"

	if err := db.Exec(stmt).Error; err != nil {
		return err
	}

	return nil
}
